import React from 'react';

export default function ProductRow(props) {
    const stockedClass = !props.stocked ? 'red-bg' : '';

    return (
        <div className={`product-row ${stockedClass}`}>
            <div className="name">{props.name}</div>
            <div className="price">{props.price}</div>
        </div>
    );
}

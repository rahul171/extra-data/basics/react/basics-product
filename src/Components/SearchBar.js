import React from 'react';

export default class SearchBar extends React.Component {
    handleSearchTextChange = (e) => {
        this.props.onSearchTextChange(e.target.value);
    }

    handleStockSettingChange = (e) => {
        this.props.onStockSettingChange(e.target.checked);
    }

    render() {
        return (
            <div className="search-bar">
                <div>
                    <label>
                        <input
                            type="checkbox"
                            value={this.props.excludeOutOfStock}
                            onChange={this.handleStockSettingChange}
                        />
                        Exclude out of stock
                    </label>
                </div>
                <div>
                    <input
                        type="text"
                        value={this.props.searchText}
                        placeholder="Search products"
                        onChange={this.handleSearchTextChange}
                    />
                </div>
            </div>
        );
    }
}

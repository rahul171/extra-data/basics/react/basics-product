import React from 'react';
import ProductList from './ProductList';

export default function ProductCategoryRow(props) {
    return (
        <div className="product-category">
            <div className="category-header">
                <div className="name">{props.name}</div>
            </div>
            <div className="product-list-wrapper">
                <ProductList products={props.products} />
            </div>
        </div>
    );
}

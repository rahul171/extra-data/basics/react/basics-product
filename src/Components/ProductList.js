import React from 'react';
import ProductRow from './ProductRow';

export default class ProductList extends React.Component {
    getProductRowsList(products) {
        return products.map((product, index) => {
            return (
                <ProductRow
                    key={index}
                    name={product.name}
                    price={product.price}
                    stocked={product.stocked}
                />
            );
        });
    }

    render() {
        return (
            <div className="product-list">
                {this.getProductRowsList(this.props.products)}
            </div>
        );
    }
}

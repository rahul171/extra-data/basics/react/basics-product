import React from 'react';
import ProductCategoryRow from './ProductCategoryRow';

export default class ProductCategoryList extends React.Component {
    getProductCategories() {
        const products = this.props.products;
        const categories = Object.keys(products);

        return categories.map((category, index) => {
            return (
                <ProductCategoryRow
                    key={index}
                    name={category}
                    products={products[category]}
                />
            );
        });
    }

    render() {
        return (
            <div className="product-category-list">
                {this.getProductCategories()}
            </div>
        );
    }
}

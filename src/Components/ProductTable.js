import React from 'react';
import ProductCategoryList from './ProductCategoryList';

export default class ProductTable extends React.Component {
    render() {
        return (
            <div className="product-table">
                <div className="product-category-list-wrapper">
                    <ProductCategoryList products={this.props.products} />
                </div>
            </div>
        );
    }
}

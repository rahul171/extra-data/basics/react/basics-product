import ReactDOM from 'react-dom';
import React from 'react';

import './index.css';
import App from './App';

import products from './data.json';

ReactDOM.render(
    <App products={products} />,
    document.getElementById('root')
);

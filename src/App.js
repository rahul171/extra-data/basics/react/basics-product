import React from 'react';
import './App.css';
import ProductTable from './Components/ProductTable';
import SearchBar from './Components/SearchBar';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchText: '',
            excludeOutOfStock: false
        };
    }

    handleSearchTextChange = (searchText) => {
        this.setState({ searchText });
    }

    handleStockSettingChange = (excludeOutOfStock) => {
        this.setState({ excludeOutOfStock });
    }

    filterProducts(categoriesObj) {
        const productName = this.state.searchText.toLowerCase();
        const excludeOutOfStock = this.state.excludeOutOfStock;

        const categories = Object.keys(categoriesObj);

        const out = {};

        for (const category of categories) {
            const filteredProducts = categoriesObj[category].filter(product => {
                return (!productName || product.name.toLowerCase().includes(productName))
                    && (!excludeOutOfStock || product.stocked);
            });

            if (filteredProducts.length !== 0) {
                out[category] = filteredProducts;
            }
        }

        return out;
    }

    render() {
        const searchText = this.state.searchText;
        const products = this.filterProducts(this.props.products);

        return (
            <div className="app">
                <div className="search-bar-wrapper">
                    <SearchBar
                        searchText={searchText}
                        excludeOutOfStock={this.state.excludeOutOfStock}
                        onSearchTextChange={this.handleSearchTextChange}
                        onStockSettingChange={this.handleStockSettingChange}
                    />
                </div>
                <div className="product-table-wrapper">
                    <ProductTable products={products} />
                </div>
            </div>
        );
    }
}
